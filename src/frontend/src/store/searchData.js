//院校相关的存储信息
const searchData = {
    namespaced:true,   //开启匿名空间
    state:{
      searchData:'',  //院校的选择列表
    },
    actions:{
      
    },
    mutations:{
      GETSEARCHDATA(state,data){
        state.searchData = data
      },
    },
    getters:{
      //返回院校的选择全部信息
      getSearchData(state){
        return state.searchData
      },
    }
}
export default searchData
